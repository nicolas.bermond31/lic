import $ from "jquery";
$(document).on('click', '#deleteUser', function() {
    let user = $('#userToDelete');
    let url = user.data('action');
    let formData = {
        'id' : user.data('id')
    }
    let ajaxManager = new AjaxManager(url, 'post', formData);
    ajaxManager.process('.usersList', '#lic_user_delete_modal');


});

$(document).on('click', '#debanUser', function() {
    let user = $('#userToDeban');
    let url = user.data('action');
    let formData = {
        'id' : user.data('id')
    }
    let ajaxManager = new AjaxManager(url, 'post', formData);
    ajaxManager.process('.usersList', '#lic_user_deban_modal');


});

$(document).on('click', '#editUser', function(){
    let url = $('#editUserForm').data('action');
    let formData = {
        'id' : $('#userId').data('value'),
        'roles': $('#role_form_roles').val()
    }
    let ajaxManager = new AjaxManager(url, 'post', formData);
    ajaxManager.process('.usersList', '#lic_user_edit_modal');
});

$(document).on('click', '#banUser', function(){
    let url = $('#banUserForm').data('action');
    let formData = {
        'id' : $('#userId').data('value'),
        'ban': $('#ban_form_ban').val()
    }
    let ajaxManager = new AjaxManager(url, 'post', formData);
    ajaxManager.process('.usersList', '#lic_user_ban_modal');
});

$(document).ready(function(){
    $('#user_rule').select2({
        theme: "bootstrap"
    });

});
