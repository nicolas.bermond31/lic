import $ from "jquery";
$(document).on('click', '#deleteHelpAsked', function() {
    let help = $('#helpToDelete');
    let url = help.data('action');
    let formData = {
        'id' : help.data('id')
    }
    let ajaxManager = new AjaxManager(url, 'post', formData);
    ajaxManager.process('.helpsList', '#lic_help_accepted_delete_modal');


});