import $ from "jquery";


$(document).ready(function() {

    $('.ajax-button').on('click', function(){
        let ajaxManager = new AjaxManager($(this).data('url'));
        ajaxManager.process();
    });

    // $('.message').each(function() {
    //     if ($(this).data('message').length > 0 ) {
    //         new NotificationManager($(this).data('message'), $(this).data('type')).display();
    //     }
    // });
});