import $ from "jquery";

$('.ajax-button').on('click', function(){
    let ajaxManager = new AjaxManager($(this).data('url'));
    ajaxManager.process();
});

$(document).on('click', '#updateProfile',  function() {
    let url = $('#lic_profile_update_modal').find('#formAction').data('action');
    let formData = {
        'id' : $('#lic_profile_update_modal').find('#formId').val(),
        'firstName' : $('#lic_profile_update_modal').find('#formFirstName').val(),
        'lastName' : $('#lic_profile_update_modal').find('#formLastName').val(),
        'token' : $('#lic_profile_update_modal').find('#formToken').val(),
    }
    let ajaxManager = new AjaxManager(url, 'post', formData);
    ajaxManager.process('.profil', '#lic_profile_update_modal');
});

$(document).on('click', '.edit-mdp', function() {
    let formId = $('#updateMdpForm');
    let url = formId.data('action');
    let formData = {
        'oldPassword' : $(document).find('#reset_password_oldPassword').val(),
        'firstPassword' : $(document).find('#reset_password_password_first').val(),
        'secondPassword' : $(document).find('#reset_password_password_second').val(),
        'token' : $(document).find('#reset_password__token').val()
    }
    let ajaxManager = new AjaxManager(url, 'post', formData);
    ajaxManager.process();
});