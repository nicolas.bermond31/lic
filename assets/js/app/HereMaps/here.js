const autosuggest = (e) => {
    if(event.metaKey) {
        return
    }
    let searchString = e.value
    if (searchString != "") {
        fetch(`https://autosuggest.search.hereapi.com/v1/autosuggest?apiKey=`+HERE_API_KEY+`&at=33.738045,73.084488&limit=5&resultType=city&q=${searchString}&lang=en-US`)
            .then(res => res.json())
            .then((json) => {
                if (json.length != 0) {
                    document.getElementById("list").innerHTML = ``;
                    let dropData = json.items.map((item) => {
                        if ((item.position != undefined) & (item.position != ""))
                            document.getElementById("list").innerHTML += `<li class="list-group-item" onClick="addValue(${item.position.lat},${item.position.lng},'${item.title}')">${item.title}</li>`;
                    });
                }
            });
    }
};

const addValue = (lat, lng, title) => {
    document.getElementById("help_asked_form_address").value =  title;
    document.getElementById("help_asked_form_lat").value =  lat;
    document.getElementById("help_asked_form_lng").value =  lng;
    document.getElementById("list").innerHTML = ``;
};