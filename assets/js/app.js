/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
// import '../css/app.css';

// Need jQuery? Install it with "yarn add jquery", then uncomment to import it.
//import $ from 'jquery';
import 'popper.js';
import 'bootstrap';
import 'select2';
import '../js/plugins/plugins';
import '../js/template/template';

import toastrImport from 'toastr';

import Vue from 'vue'
import  Datetime  from 'vue-datetime'
// import VCalendar from 'v-calendar'
import axios from 'axios'
import VueAxios from 'vue-axios'
import VueNotification from "@kugatsu/vuenotification"

//css
import 'vue-datetime/dist/vue-datetime.css'
import "leaflet/dist/leaflet.css";

delete L.Icon.Default.prototype._getIconUrl;

L.Icon.Default.mergeOptions({
    iconRetinaUrl: require('leaflet/dist/images/marker-icon-2x.png'),
    iconUrl: require('leaflet/dist/images/marker-icon.png'),
    shadowUrl: require('leaflet/dist/images/marker-shadow.png'),
});

const options = {
    position: 'bottomRight',
    zIndex: 9999,
    timer: 10,
    showCloseIcn: true
}
Vue.use(VueAxios, axios);
Vue.use(VueNotification, options)
Vue.use(Datetime)
// Vue.use(VCalendar, {
//     componentPrefix: 'vc'
// })
//$.fn.select2.defaults.set( "theme", "bootstrap" );

// require('@fortawesome/fontawesome-free/css/all.min.css');
// require('@fortawesome/fontawesome-free/js/all.min');

//let toastr = window.toastr = toastrImport;

//components
import MapItem from "../components/MapItem";
import ButtonItem from '../components/ButtonItem';
import ButtonSimpleItem from "../components/ButtonSimpleItem";
import HelpItem from "../components/HelpItem";
import RemarkItem from "../components/RemarkItem";
import FormRemarkItem from "../components/FormRemarkItem"
import FormHelp from "../components/FormHelp";
import Calendar from "../components/Calendar"
import store from "../vue/store/index"
import HelpAsked from "../components/HelpAsked"
import AddHelp from "../components/AddHelp";
import HelpAccepted from "../components/HelpAccepted";

Vue.component('map-item', MapItem)
Vue.component('button-item', ButtonItem)
Vue.component('button-simple-item', ButtonSimpleItem)
Vue.component('help-item', HelpItem)
Vue.component('remark-item', RemarkItem)
Vue.component('form-remark-item', FormRemarkItem)
Vue.component('Calendar', Calendar)
Vue.component('form-help', FormHelp)
Vue.component('help-asked', HelpAsked)
Vue.component('add-help', AddHelp)
Vue.component('help-accepted', HelpAccepted)

new Vue({
    el: '#app',
    store
})