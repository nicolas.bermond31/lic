import $ from 'jquery';

window.AjaxManager = (function () {
    'use strict';

    AjaxManager.build = function(url, method, params) {


        if (typeof(params) == 'undefined') {
            params = {};
        }

        if (typeof(method) == 'undefined') {
            method = 'get';
        }


        return new AjaxManager(url, method, params);
    };

    function AjaxManager(url, method, params) {
        this.url = url;
        this.params = params;
        this.method = method;
    }

    AjaxManager.prototype = {
        process: function (element,modal) {
            $.ajax({
                'url': this.url,
                'type': this.method,
                'data' : this.params,
                'success' : function(data) {
                    if (data.hasOwnProperty('html')) {
                        if (data.success) {
                            $(document).find(element).empty();
                            $(document).find(element).append(data.html);
                           // new NotificationManager(data.message, 'success').display();
                        } else {
                          //  new NotificationManager(data.message, 'error').display();
                        }
                        $(document).find(modal).modal('hide');
                    } else if(data.hasOwnProperty('noModal')) {
                        if (data.success) {
                            //new NotificationManager(data.message, 'success').display();
                        } else {
                            new NotificationManager(data.message, 'error').display();
                        }
                    } else if (data.hasOwnProperty('modal')) {
                        if (data.success) {
                            //new NotificationManager(data.message, 'success').display();
                        } else {
                           // new NotificationManager(data.message, 'error').display();
                        }
                        $(document).find(modal).modal('hide');
                    }
                    else {
                        let $modal = $(data);
                        $('body').append($modal);
                        $modal.modal('show');
                    }

                }
            });
        },
    };
    return AjaxManager;
}());