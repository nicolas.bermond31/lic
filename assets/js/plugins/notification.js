window.NotificationManager = (function () {
    'use strict';

    NotificationManager.build = function (message, type) {
        return new NotificationManager(message, type);
    };

    function NotificationManager(message, type) {

        this.message = message;
        this.type    = type;

        if (this.type === 'danger')
            this.type = 'error';

        this.timeout = 5000;

    }

    NotificationManager.prototype = {

        display: function () {

            toastr.options = {
                "closeButton": false,
                "debug": false,
                "newestOnTop": true,
                "progressBar": true,
                "positionClass": "toast-top-right",
                "preventDuplicates": true,
                "onclick": null,
                "showDuration": 800,
                "hideDuration": 100,
                "timeOut": this.timeout,
                "extendedTimeOut": 1000,
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            };

            toastr[this.type](this.message);

        },

    };

    return NotificationManager;

}());