import  Vue from "vue"
import Vuex from "vuex"
import RemarkModule from "./remark"
import HelpModule from './help'
import HelpAcceptedModule from "./helpAccepted";

Vue.use(Vuex)

export default new Vuex.Store({
    modules: {
        remark: RemarkModule,
        help: HelpModule,
        helpAccepted: HelpAcceptedModule
    }
})