import HelpAcceptedApi from "../api/helpAccepted"

const FETCHING_HELP = "FETCHING_HELP",
    FETCHING_HELP_SUCCESS = "FETCHING_HELP_SUCCESS",
    FETCHING_HELP_ERROR = 'FETCHING_HELP_ERROR';

export default {
    namespaced: true,
    state: {
        isLoading: false,
        error: null,
        helps: []
    },
    getters: {
        isLoading(state) {
            return state.isLoading
        },
        hasError(state) {
            return state.error !== null
        },
        error(state) {
            return state.error
        },
        helps(state) {
            console.log(state.helps)
            return state.helps
        },
        hasHelps(state) {
            if (state.helps.length > 0) {
                return true
            }
            else {
                return false
            }
        },
    },
    mutations: {
        [FETCHING_HELP](state) {
            state.isLoading = true
            state.error = null
            state.helps = []
        },
        [FETCHING_HELP_SUCCESS](state,helps) {
            state.isLoading = false
            state.error = null
            state.helps = helps
        },
        [FETCHING_HELP_ERROR](state, error) {
            state.isLoading = false
            state.error = error
            state.helps = []
        },
    },
    actions: {
        async findAllAccepted({commit}) {
            commit(FETCHING_HELP)
            try {
                let response = await HelpAcceptedApi.findAllAccepted()
                commit(FETCHING_HELP_SUCCESS, response.data)
                return response.data
            }
            catch (error) {
                commit(FETCHING_HELP_ERROR, error)
                return null
            }
        }
    }

}