import RemarkApi from "../api/remark"

const CREATING_REMARK = "CREATING_REMARK",
    CREATING_REMARK_SUCCESS = "CREATING_REMARK_SUCCESS",
    CREATING_REMARK_ERROR = "CREATING_REMARK_ERROR",
    FETCHING_REMARKS = "FETCHING_REMARKS",
    FETCHING_REMARKS_SUCCESS = "FETCHING_REMARKS_SUCCESS",
    FETCHING_REMARKS_ERROR = 'FETCHING_REMARKS_ERROR';

export default {
    namespaced: true,
    state: {
        isLoading: false,
        error: null,
        remarks: []
    },
    getters: {
        isLoading(state) {
            return state.isLoading
        },
        hasError(state) {
            return state.error !== null
        },
        error(state) {
            return state.error
        },
        hasRemarks(state) {
            if (state.remarks.remarks.length > 0) {
                return true
            }
            else {
                return false
            }
        },
        remarks(state) {
            return state.remarks
        }
    },
    mutations: {
        [CREATING_REMARK](state) {
            state.isLoading = true
            state.error = null
        },
        [CREATING_REMARK_SUCCESS](state, remark) {
            state.isLoading = false
            state.error = null
            state.remarks.remarks.unshift(remark)
        },
        [CREATING_REMARK_ERROR](state, error) {
            state.isLoading = false
            state.error = error
            state.remarks = []
        },
        [FETCHING_REMARKS](state) {
            state.isLoading = true
            state.error = null
            state.remarks = []
        },
        [FETCHING_REMARKS_SUCCESS](state,remarks) {
            state.isLoading = false
            state.error = null
            state.remarks = remarks
        },
        [FETCHING_REMARKS_ERROR](state, error) {
            state.isLoading = false
            state.error = error
            state.remarks = []
        },
    },
    actions: {
        async create({commit},payload) {
            commit(CREATING_REMARK)
            try{
                let response = await RemarkApi.create(payload.content, payload.helpId)
                commit(CREATING_REMARK_SUCCESS, response.data)
                return response.data
            }
            catch (error) {
                commit(CREATING_REMARK_ERROR, error)
                return null
            }
        },
        async findAll({commit}, id) {
            commit(FETCHING_REMARKS)
            try {
                let response = await RemarkApi.findAll(id)
                commit(FETCHING_REMARKS_SUCCESS, response.data)
                return response.data
            }
            catch (error) {
                commit(FETCHING_REMARKS_ERROR, error)
                return null
            }
        }
    }

}