import HelpApi from "../api/help"

const CREATING_HELP = "CREATING_HELP",
    CREATING_HELP_SUCCESS = "CREATING_HELP_SUCCESS",
    CREATING_HELP_ERROR = "CREATING_HELP_ERROR",
    FETCHING_HELP = "FETCHING_HELP",
    FETCHING_HELP_SUCCESS = "FETCHING_HELP_SUCCESS",
    FETCHING_HELP_ERROR = 'FETCHING_HELP_ERROR';

export default {
    namespaced: true,
    state: {
        isLoading: false,
        error: null,
        helps: []
    },
    getters: {
        isLoading(state) {
            return state.isLoading
        },
        hasError(state) {
            return state.error !== null
        },
        error(state) {
            return state.error
        },
        helps(state) {
            return state.helps.helps
        },
        hasHelps(state) {
            if (state.helps.helps.length > 0) {
                return true
            }
            else {
                return false
            }
        },
    },
    mutations: {
        [CREATING_HELP](state) {
            state.isLoading = true
            state.error = null
        },
        [CREATING_HELP_SUCCESS](state) {
            state.isLoading = false
            state.error = null
        },
        [CREATING_HELP_ERROR](state, error) {
            state.isLoading = false
            state.error = error
            state.helps = []
        },
        [FETCHING_HELP](state) {
            state.isLoading = true
            state.error = null
            state.helps = []
        },
        [FETCHING_HELP_SUCCESS](state,helps) {
            state.isLoading = false
            state.error = null
            state.helps = helps
        },
        [FETCHING_HELP_ERROR](state, error) {
            state.isLoading = false
            state.error = error
            state.helps = []
        },
    },
    actions: {
        async create({commit},payload) {
            commit(CREATING_HELP)
            try{
                let response = await HelpApi.create(
                    payload.title,
                    payload.content,
                    payload.address,
                    payload.lat,
                    payload.lng,
                    payload.dateHelp
                )
                commit(CREATING_HELP_SUCCESS, response.data)
                return response.data
            }
            catch (error) {
                commit(CREATING_HELP_ERROR, error)
                return null
            }
        },
        async findAll({commit}) {
            commit(FETCHING_HELP)
            try {
                let response = await HelpApi.findAll()
                commit(FETCHING_HELP_SUCCESS, response.data)
                return response.data
            }
            catch (error) {
                commit(FETCHING_HELP_ERROR, error)
                return null
            }
        }
    }

}