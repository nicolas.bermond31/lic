import axios from "axios"

export default {

    findAllAccepted() {
        return axios.get("/lic/public/help/accepted/items/")
    }
}