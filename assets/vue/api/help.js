import axios from "axios"

export default {
    create(title, content, address, lat, lng, dateHelp) {
        let formData = new FormData()
        formData.append('title',title)
        formData.append('content',content)
        formData.append('address',address)
        formData.append('lat',lat)
        formData.append('lng',lng)
        formData.append('dateHelp',dateHelp)
        return axios({
            method: 'post',
            url: "/lic/public/help/asked/store",
            data: formData,
            headers: {'Content-Type': 'multipart/form-data'}
        });
    },
    findAll(id) {
        return axios.get("/lic/public/help/asked/items/")
    }
}