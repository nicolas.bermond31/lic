import axios from "axios"

export default {
    create(message, id) {
        let formData = new FormData()
        formData.append('content',message)
        formData.append('helpId', id)
        return axios({
            method: 'post',
            url: "/lic/public/remark/store",
            data: formData,
            headers: {'Content-Type': 'multipart/form-data'}
        });
    },
    findAll(id) {
        return axios.get("/lic/public/help/accepted/remark/items/"+id)
    }
}