<?php

namespace App\Repository;

use App\Entity\HelpAccepted;
use App\Entity\HelpAsked;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method HelpAsked|null find($id, $lockMode = null, $lockVersion = null)
 * @method HelpAsked|null findOneBy(array $criteria, array $orderBy = null)
 * @method HelpAsked[]    findAll()
 * @method HelpAsked[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HelpAskedRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, HelpAsked::class);
    }

    public function findHelpsByStatusAndUser($isAccepted, $user)
    {
        $subQb = $this->createQueryBuilder('help_accepted')
            ->from(HelpAccepted::class,'ha')
            ->leftJoin('ha.help','hp')
            ->leftJoin('ha.acceptedBy','u')
            ->where('u.id <> :userId')

            ->select('hp.id')
            ->getDQL();

        $exper = $this->_em->getExpressionBuilder();

        $qb =  $this->createQueryBuilder('h');
            if ($isAccepted) {
                $qb->where(
                    $exper->in(
                        'h.id',
                        $subQb
                    )
                );
            }
            else {
                $qb->where(
                    $exper->notIn(
                        'h.id',
                        $subQb
                    )
                );
            }

            $qb->andWhere('h.user = :val')
            ->setParameter('val', $user)
            ->setParameter('userId',$user->getId())
            ->orderBy('h.id', 'ASC');

            return $qb
            ->getQuery()
            ->getResult()
            ;
    }

    /**
     * @param User $user
     * @return int|mixed|string
     */
    public function findNotMyUser(User $user)
    {
        $subQb = $this->createQueryBuilder('help_accepted')
            ->from(HelpAccepted::class,'ha')
            ->leftJoin('ha.help','hp')
            ->select('hp.id')
            ->getDQL();
        $exper = $this->_em->getExpressionBuilder();

        return $this->createQueryBuilder('h')
            ->where(
                $exper->notIn(
                    'h.id',
                    $subQb
                )
            )
            ->andWhere('h.user <> :val')
            ->setParameter('val', $user)
            ->orderBy('h.id', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }


    /*
    public function findOneBySomeField($value): ?HereMaps
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
