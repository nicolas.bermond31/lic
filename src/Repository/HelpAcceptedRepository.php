<?php

namespace App\Repository;

use App\Entity\HelpAccepted;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method HelpAccepted|null find($id, $lockMode = null, $lockVersion = null)
 * @method HelpAccepted|null findOneBy(array $criteria, array $orderBy = null)
 * @method HelpAccepted[]    findAll()
 * @method HelpAccepted[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HelpAcceptedRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, HelpAccepted::class);
    }

    /**
     * @param $userId
     * @param $status
     * @return HelpAccepted[] Returns an array of HelpAccepted objects
     */
    public function findByUserAndStatus($user, $status)
    {
        $qb=  $this->createQueryBuilder('ha')
            ->leftJoin('ha.help', 'h')
            ->where('h.user = :user');
            if (empty($status)){
                $qb->andWhere($qb->expr()->isNull('ha.deleted'));
            } else {
                $qb->andWhere('ha.deleted = :status')
                    ->setParameter('status', $status);
            }

            $qb->setParameter('user', $user)

            ->orderBy('h.id', 'ASC');
//            ->setMaxResults(10)
          return  $qb->getQuery()
            ->getResult()
        ;
    }

    /*
    public function findOneBySomeField($value): ?HelpAccepted
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
