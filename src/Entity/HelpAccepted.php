<?php

namespace App\Entity;

use App\Repository\HelpAcceptedRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=HelpAcceptedRepository::class)
 */
class HelpAccepted
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $acceptedBy;

    /**
     * @ORM\ManyToOne(targetEntity=HelpAsked::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $help;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deleted;

    /**
     * @ORM\OneToMany(targetEntity=Remark::class, mappedBy="helpAccepted")
     */
    private $remarksAccepted;


    public function __construct()
    {
        $this->created = new \DateTime();
        $this->remarksAccepted = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAcceptedBy(): ?User
    {
        return $this->acceptedBy;
    }

    public function setAcceptedBy(?User $acceptedBy): self
    {
        $this->acceptedBy = $acceptedBy;

        return $this;
    }

    public function getHelp(): ?HelpAsked
    {
        return $this->help;
    }

    public function setHelp(?HelpAsked $help): self
    {
        $this->help = $help;

        return $this;
    }

    public function getCreated(): ?\DateTimeInterface
    {
        return $this->created;
    }

    public function setCreated(\DateTimeInterface $created): self
    {
        $this->created = $created;

        return $this;
    }

    public function getDeleted(): ?\DateTimeInterface
    {
        return $this->deleted;
    }

    public function setDeleted(?\DateTimeInterface $deleted): self
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * @return Collection|Remark[]
     */
    public function getRemarksAccepted(): Collection
    {
        return $this->remarksAccepted;
    }

    public function addRemarksAccepted(Remark $remarksAccepted): self
    {
        if (!$this->remarksAccepted->contains($remarksAccepted)) {
            $this->remarksAccepted[] = $remarksAccepted;
            $remarksAccepted->setHelpAccepted($this);
        }

        return $this;
    }

    public function removeRemarksAccepted(Remark $remarksAccepted): self
    {
        if ($this->remarksAccepted->contains($remarksAccepted)) {
            $this->remarksAccepted->removeElement($remarksAccepted);
            // set the owning side to null (unless already changed)
            if ($remarksAccepted->getHelpAccepted() === $this) {
                $remarksAccepted->setHelpAccepted(null);
            }
        }

        return $this;
    }

   
}
