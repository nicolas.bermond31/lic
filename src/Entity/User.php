<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @UniqueEntity(fields={"email"}, message="There is already an account with this email")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     * @Assert\Email(
     *     message = "L'email '{{ value }}' n'est pas un email valide.",
     *     mode = "html5"
     * )
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=180, nullable=true)
     * @Assert\NotBlank(message="Le prénom ne peut être vide")
     * @Assert\Length(
     *     min=2,
     *     minMessage="Le prénom doit au moins contenir {{ limit }} caractères.",
     *     max=180,
     *     maxMessage="Le prénom ne doit pas dépasser {{ limit }} caractères.")
     * @Assert\Regex(pattern="/\d/", match=false, message="Le prénom ne doit comporter que des caractères alphabétiques ainsi que '-' ou des espaces")
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=180, nullable=true)
     * @Assert\NotBlank(message="Le prénom ne peut être vide")
     * @Assert\Length(
     *     min=2,
     *     minMessage="Le nom doit au moins contenir {{ limit }} caractères.",
     *     max=180,
     *     maxMessage="Le nom ne doit pas dépasser {{ limit }} caractères.")
     * @Assert\Regex(pattern="/\d/", match=false, message="Le nom ne doit comporter que des caractères alphabétiques ainsi que '-' ou des espaces")
     */
    private $lastName;

    /**
     * @ORM\Column(type="boolean" , options={"default" : false})
     */
    private $temporaryBanned;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $dateToDeban;

    /**
     * @ORM\Column(type="boolean" , options={"default" : false})
     */
    private $banned;

    /**
     * @ORM\Column(type="integer")
     */
    private $nbBan;

    /**
     * @ORM\OneToMany(targetEntity=HelpAsked::class, mappedBy="user")
     */
    private $helpAskeds;

    public function __construct()
    {
        $this->helpAskeds = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param mixed $firstName
     */
    public function setFirstName($firstName): void
    {
        $this->firstName = $firstName;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param mixed $lastName
     */
    public function setLastName($lastName): void
    {
        $this->lastName = $lastName;
    }

    /**
     * @return mixed
     */
    public function getTemporaryBanned()
    {
        return $this->temporaryBanned;
    }

    /**
     * @param mixed $temporaryBanned
     */
    public function setTemporaryBanned($temporaryBanned): void
    {
        $this->temporaryBanned = $temporaryBanned;
    }

    /**
     * @return mixed
     */
    public function getDateToDeban()
    {
        return $this->dateToDeban;
    }

    /**
     * @param mixed $dateToDeban
     */
    public function setDateToDeban($dateToDeban): void
    {
        $this->dateToDeban = $dateToDeban;
    }

    /**
     * @return mixed
     */
    public function getBanned()
    {
        return $this->banned;
    }

    /**
     * @param mixed $banned
     */
    public function setBanned($banned): void
    {
        $this->banned = $banned;
    }

    /**
     * @return mixed
     */
    public function getNbBan()
    {
        return $this->nbBan;
    }

    /**
     * @param mixed $nbBan
     */
    public function setNbBan($nbBan): void
    {
        $this->nbBan = $nbBan;
    }

    public function fullName()
    {
        return $this->getFirstName().' '.$this->getLastName();
    }

    /**
     * @return Collection|HelpAsked[]
     */
    public function getHelpAskeds(): Collection
    {
        return $this->helpAskeds;
    }

    public function addHelpAsked(HelpAsked $helpAsked): self
    {
        if (!$this->helpAskeds->contains($helpAsked)) {
            $this->helpAskeds[] = $helpAsked;
            $helpAsked->setUser($this);
        }

        return $this;
    }

    public function removeHelpAsked(HelpAsked $helpAsked): self
    {
        if ($this->helpAskeds->contains($helpAsked)) {
            $this->helpAskeds->removeElement($helpAsked);
            // set the owning side to null (unless already changed)
            if ($helpAsked->getUser() === $this) {
                $helpAsked->setUser(null);
            }
        }

        return $this;
    }


}
