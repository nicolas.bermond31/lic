<?php

namespace App\Entity;

use App\Repository\RemarkRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=RemarkRepository::class)
 */
class Remark
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity=HelpAccepted::class, inversedBy="remarksAccepted")
     * @ORM\JoinColumn(nullable=false)
     */
    private $helpAccepted;

    /**
     * @ORM\Column(type="text")
     */
    private $content;


    public function __construct()
    {
        $this->created = new \DateTime();
    }
    /**
     * @ORM\Column(type="datetime")
     */
    private $created;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getHelpAccepted(): ?HelpAccepted
    {
        return $this->helpAccepted;
    }

    public function setHelpAccepted(?HelpAccepted $helpAccepted): self
    {
        $this->helpAccepted = $helpAccepted;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getCreated(): ?\DateTimeInterface
    {
        return $this->created;
    }

    public function setCreated(\DateTimeInterface $created): self
    {
        $this->created = $created;

        return $this;
    }
}
