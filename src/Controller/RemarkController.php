<?php

namespace App\Controller;

use App\Entity\HelpAccepted;
use App\Entity\Remark;
use App\Form\RemarkType;
use App\Service\FormatService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class RemarkController extends AbstractController
{

    /**
     * @Route("/help/accepted/remark/items/{id}", name="remark_ajax")
     * @param FormatService $formatService
     * @param $id
     * @return JsonResponse
     */
    public function getRemarks(FormatService $formatService, $id): JsonResponse
    {
        $helpAccepted = $this->getDoctrine()->getManager()->getRepository(HelpAccepted::class)->find($id);
        if (!empty($helpAccepted))
            $remarks = $this->getDoctrine()->getManager()->getRepository(Remark::class)->findBy(['helpAccepted' => $helpAccepted]);
        else
            $remarks = [];

        $arrayRemarks = $formatService->renderArrayRemarks($remarks, $this->getUser());

        return new JsonResponse(['remarks' => $arrayRemarks, 'helpAcceptedId' => $id]);
    }

    /**
     * @Route("/remark/render/form/{id}", name="render_form_remark")
     */
    public function renderForm($id)
    {
        $em = $this->getDoctrine()->getManager();
        $helpAccepted = $em->getRepository(HelpAccepted::class)->find($id);
        $remark = new Remark();
        $remark->setHelpAccepted($helpAccepted);
        $remark->setUser($this->getUser());
        $form = $this->createForm(RemarkType::class,$remark);

        $view = $this->renderView('remark/form.html.twig', [
            'form' => $form->createView()
        ]);
        $arrayModel = ['content'];

        return new JsonResponse([
            'form' => $view,
            'models' => $arrayModel
        ]);

    }

    /**
     * @Route("/remark/store", name="store_remark")
     */
    public function storeRemark(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $helpAccepted = $em->getRepository(HelpAccepted::class)->find($request->request->get('helpId'));
        $remark = new Remark();
        $remark->setHelpAccepted($helpAccepted);
        $remark->setUser($this->getUser());
        $remark->setContent($request->request->get('content'));
        $em->persist($remark);
        $em->flush();

        return new JsonResponse(['message' => 'Remarque bien enregistrée']);

    }

    /**
     * @Route("/remark", name="remark")
     */
    public function index()
    {
        return $this->render('remark/index.html.twig', [
            'controller_name' => 'RemarkController',
        ]);
    }
}
