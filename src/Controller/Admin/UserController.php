<?php


namespace App\Controller\Admin;


use App\Entity\User;
use App\Form\BanFormType;
use App\Form\RoleFormType;
use App\Service\SendMailService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends AbstractController
{
    /**
     * @Route("/admin/user/delete/modal/{id}/{action}", name="admin_user_modal")
     * @param $id
     * @param $action
     * @return Response
     */
    public function generateModal($id, $action)
    {
        $em = $this->getDoctrine()->getManager();

        $params = [];
        $user = $em->getRepository(User::class)->find($id);
        if ($action === 'edit') {
            $form = $this->createForm(RoleFormType::class);
            $params = [
                'user' => $user,
                'modalId' => 'lic_user_'.$action.'_modal',
                'form' => $form->createView()
            ];
        }
        else if ($action === 'ban') {
            $form = $this->createForm(BanFormType::class);
            $params = [
                'user' => $user,
                'modalId' => 'lic_user_'.$action.'_modal',
                'form' => $form->createView()
            ];
        }
        else if ($action === 'deban') {
            $params = [
                'user' => $user,
                'modalId' => 'lic_user_'.$action.'_modal',
            ];
        }
        else {
            $params = [
                'user' => $user,
                'modalId' => 'lic_user_'.$action.'_modal'
            ];
        }

        $html = $this->renderView('admin/Modal/'.$action.'.html.twig', $params);

        return new Response($html);
    }

    /**
     * @Route("/admin/delete/user", name="admin_delete_user")
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function deleteUser(Request $request)
    {
        if ($request->isXmlHttpRequest()) {

            $em = $this->getDoctrine()->getManager();
            $user = $em->getRepository(User::class)->find($request->request->get('id'));

            if (!empty($user)) {
                $em->remove($user);
                $em->flush();
            }
            $users = $em->getRepository(User::class)->findAll();
            $html = $this->renderView('admin/Widget/users.html.twig', [
                'users' => $users
            ]);
            return new JsonResponse(['success' => true, 'html' => $html, 'message' => 'Utilisateur bien supprimé']);

        }
        return new JsonResponse(['success' => false, 'message' => 'Impossible de supprimer l\'utilisateur']);

    }

    /**
     * @Route("/admin/deban/user", name="admin_deban_user")
     *
     * @param Request $request
     * @param SendMailService $sendEmailService
     * @return JsonResponse
     */
    public function debanUser(Request $request, SendMailService $sendEmailService)
    {
        if ($request->isXmlHttpRequest()) {

            $em = $this->getDoctrine()->getManager();
            $user = $em->getRepository(User::class)->find($request->request->get('id'));

            if (!empty($user)) {
                $user->setTemporaryBanned(false);
                $user->setNbBan(0);
                $user->setDateToDeban(null);
                $user->setBanned(false);
                $em->persist($user);
                $em->flush();
            }

            $htmlMail = $this->renderView('security/mailDeban.html.twig', ['user' => $user]);

            $message = $sendEmailService->sendEmail($user->getEmail(), $htmlMail, 'Bannissement du LIC');

            $users = $em->getRepository(User::class)->findAll();
            $html = $this->renderView('admin/Widget/users.html.twig', [
                'users' => $users
            ]);

            return new JsonResponse(['success' => true, 'html' => $html, 'message' => 'L\'utilisateur a de nouveau accès au site']);

        }
        return new JsonResponse(['success' => false, 'message' => 'Impossible de débannir l\'utilisateur']);

    }

    /**
     * @Route("/admin/ban/user", name="admin_ban_user")
     *
     * @param Request $request
     * @param SendMailService $sendEmailService
     * @return JsonResponse
     */
    public function banUser(Request $request, SendMailService $sendEmailService)
    {
        if ($request->isXmlHttpRequest()) {
            $em = $this->getDoctrine()->getManager();
            $user = $em->getRepository(User::class)->find($request->request->get('id'));
            $typeBan = $request->request->get('ban');

            if ($typeBan === 'forever') {
                $user->setTemporaryBanned(false);
                $user->setBanned(true);
                $em->persist($user);
            }
            else {
                switch ($user->getNbBan()) {
                    case 0:
                        $datDeban = new \DateTime();
                        $datDeban->modify('+1 day');
                        $user->setTemporaryBanned(true);
                        $user->setDateToDeban($datDeban);
                        $user->setNbBan(1);
                        break;
                    case 1:
                        $datDeban = new \DateTime();
                        $datDeban->modify('+15 day');
                        $user->setTemporaryBanned(true);
                        $user->setDateToDeban($datDeban);
                        $user->setNbBan(2);
                        break;
                    case 2:
                        $datDeban = new \DateTime();
                        $datDeban->modify('+30 day');
                        $user->setTemporaryBanned(true);
                        $user->setDateToDeban($datDeban);
                        $user->setNbBan(3);
                        break;
                    case 3:
                        $user->setTemporaryBanned(false);
                        $user->setBanned(true);
                        break;
                }
                $em->persist($user);
            }
            $em->flush();
            $html = $this->renderView('security/mailBan.html.twig', ['user' => $user]);

            $message = $sendEmailService->sendEmail($user->getEmail(), $html, 'Bannissement du LIC');

            return new JsonResponse(['success' => true, 'modal' => true, 'message' => 'Bannisement bien pris en compte.']);
        }
        return new JsonResponse(['success' => false, 'modal' => true,'message' => 'Impossible de bannir l\'utilisateur.']);
    }

    /**
     * @Route("/admin/edit/user/roles", name="admin_edit_user_roles")
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function editUserRoles(Request $request)
    {
        if ($request->isXmlHttpRequest()) {

            $em = $this->getDoctrine()->getManager();
            $user = $em->getRepository(User::class)->find($request->request->get('id'));


            if (!empty($user)) {
                $user->setRoles($request->request->get('roles'));
                $em->persist($user);
                $em->flush();
            }
            $users = $em->getRepository(User::class)->findAll();
            $html = $this->renderView('admin/Widget/users.html.twig', [
                'users' => $users
            ]);
            return new JsonResponse(['success' => true, 'html' => $html, 'message' => 'Les rôles de l\'utilisateur ont bien été mis à jour.']);

        }
        return new JsonResponse(['success' => false, 'message' => 'Impossible de mettre à jour les rôles de l\'utilisateur']);

    }
}