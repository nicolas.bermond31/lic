<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\ResetPasswordType;
use App\Model\ChangePassword;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class ProfileController extends AbstractController
{
    /**
     * @Route("/profile", name="profile")
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();

        $changePassword = new ChangePassword();
        $form = $this->createForm(ResetPasswordType::class, $changePassword);

        return $this->render('profile/index.html.twig', [
            'form' => $form->createView(),
            'user' => $user,
        ]);
    }

    /**
     * @Route("/updateMDP", name="update_mdp")
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @return JsonResponse
     */
    public function updateMDP(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        if ($request->isXmlHttpRequest()) {
            $submittedToken = $request->request->get('token');

            if ($this->isCsrfTokenValid('update-mdp', $submittedToken)) {
                $em = $this->getDoctrine()->getManager();
                $user = $this->getUser();

                if (!empty($user)) {
                    $oldPassword = $request->request->get('oldPassword');
                    $firstPaswword = $request->request->get('firstPassword');
                    $secondPassword = $request->request->get('secondPassword');
                    if ($passwordEncoder->isPasswordValid($user, $oldPassword)) {
                        if ($firstPaswword === $secondPassword) {
                            $newEncodedPassword = $passwordEncoder->encodePassword($user, $firstPaswword);
                            $user->setPassword($newEncodedPassword);
                            $em->persist($user);
                            $em->flush();
                            return new JsonResponse(['success' => true, 'noModal' => true, 'message' => 'Mot de passé modifié']);
                        } else {
                            return new JsonResponse(['success' => false, 'noModal' => true, 'message' => 'Les mots de passes ne correspondent pas']);
                        }
                    }
                    else {
                        return new JsonResponse(['success' => false, 'noModal' => true, 'message' => 'Mauvais mot de passe !!']);
                    }

                } else {
                    return new JsonResponse(['success' => false, 'noModal' => true, 'message' => 'Vous devez être connecté pour modifier votre mot de passe']);
                }
            }
        }
        return new JsonResponse(['success' => false, 'noModal' => true, 'message' => 'Impossible de mettre à jour le mot de passe']);
    }

    /**
     * @Route("/profile/edit/{id}", name="edit_profile")
     * @param $id
     * @return Response
     */
    public function generateUpdateModal($id)
    {
        $em = $this->getDoctrine()->getManager();

        $user = $em->getRepository(User::class)->find($id);
        $html = $this->renderView('profile/Modal/update.html.twig', [
            'user' => $user,
            'modalId' => 'lic_profile_update_modal'
        ]);

        return new Response($html);
    }

    /**
     * @Route("/profile/update", name="update_profile")
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function updateProfile(Request $request)
    {
        if ($request->isXmlHttpRequest()) {
            $submittedToken = $request->request->get('token');

            if ($this->isCsrfTokenValid('update-profile', $submittedToken)) {
                $em = $this->getDoctrine()->getManager();
                $user = $em->getRepository(User::class)->find($request->request->get('id'));

                if (!empty($user)) {
                    $user->setFirstName($request->request->get('firstName'));
                    $user->setLastName($request->request->get('lastName'));
                    $em->persist($user);
                    $em->flush();
                }

                $html = $this->renderView('profile/Widget/profile.html.twig', ['user' => $user]);

                return new JsonResponse([
                    'success' => true,
                    'html' => $html,
                    'message' => 'Votre profil a bien été mis à jour']
                );
            }
        }
        return new JsonResponse(['success' => false, 'message' => 'Impossible de mettre à jour le profil']);

    }
}
