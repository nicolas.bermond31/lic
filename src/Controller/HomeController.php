<?php


namespace App\Controller;


use App\Entity\HelpAsked;
use App\Service\FormatService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/homepage/helps", name="homepage_helps_ajax")
     * @param FormatService $formatService
     * @return JsonResponse
     */
    public function getHelpsHomepage(FormatService $formatService): JsonResponse
    {
        $helps = (!empty($this->getUser())) ?$this->getDoctrine()->getRepository(HelpAsked::class)->findNotMyUser($this->getUser()) : null;
        $arrayHelps = $formatService->renderArrayHelps($helps);

        return new JsonResponse($arrayHelps);
    }

    /**
     * @Route("/", name="homepage")
     * @return Response
     */
    public function homepage()
    {
        $helps = (!empty($this->getUser())) ?$this->getDoctrine()->getRepository(HelpAsked::class)->findNotMyUser($this->getUser()) : null;
        return $this->render('home/index.html.twig', [
            'helps' => $helps
        ]);
    }
}