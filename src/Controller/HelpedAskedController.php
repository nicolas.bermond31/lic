<?php

namespace App\Controller;

use App\Entity\Address;
use App\Entity\HelpAccepted;
use App\Entity\HelpAsked;
use App\Form\HelpAskedFormType;
use App\Service\FormatService;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HelpedAskedController extends AbstractController
{

    /**
     * @Route("/helped/asked", name="helped_asked")
     */
    public function index()
    {
        $em = $this->getDoctrine()->getManager();
        $helpsAccepted = $em->getRepository(HelpAsked::class)->findHelpsByStatusAndUser(true, $this->getUser());
        $helpsNotAccepted = $em->getRepository(HelpAsked::class)->findHelpsByStatusAndUser(false, $this->getUser());

        return $this->render('helped_asked/index.html.twig', [
            'helpsAccepted' => $helpsAccepted,
            'helpsNotAccepted' => $helpsNotAccepted,
        ]);
    }

    /**
     * @Route("/help/accepted/items", name="helps_accepted_ajax")
     * @param FormatService $formatService
     * @return JsonResponse
     */
    public function getHelpsAccepted(FormatService $formatService): JsonResponse
    {
        $helps = $this->getDoctrine()->getManager()->getRepository(HelpAccepted::class)->findByUserAndStatus($this->getUser(), null);
        $arrayHelps = $formatService->renderArrayHelps($helps, 'accepted');

        return new JsonResponse($arrayHelps);
    }

    /**
     * @Route("/help/accepted-by-me/items", name="helps_accepted_by_me_ajax")
     * @param FormatService $formatService
     * @return JsonResponse
     */
    public function getHelpsAcceptedByMe(FormatService $formatService): JsonResponse
    {
        $helps = $this->getDoctrine()->getManager()->getRepository(HelpAccepted::class)->findBy(['acceptedBy' => $this->getUser(), 'deleted' => null]);
        $arrayHelps = $formatService->renderArrayHelps($helps, 'accepted');

        return new JsonResponse($arrayHelps);
    }

    /**
     * @Route("/help/accepted/calendar/items", name="helps_accepted_calendar_ajax")
     * @return JsonResponse
     */
    public function getHelpsForCalendar(): JsonResponse
    {
        $helpsAccepted = $this->getDoctrine()->getManager()->getRepository(HelpAccepted::class)->findBy(['acceptedBy' => $this->getUser(), 'deleted' => null]);
        $results = [];
        foreach ($helpsAccepted as $helpAccepted) {
            if (!empty($helpAccepted->getHelp()->getDateHelp())) {
                $results[] = [
                    'title' => $helpAccepted->getHelp()->getTitle(),
                    'start' => $helpAccepted->getHelp()->getDateHelp()->format(\DateTime::ATOM),
                    'end' => $helpAccepted->getHelp()->getDateHelp()->format(\DateTime::ATOM)
                ];
            }
        }

        return new JsonResponse($results);
    }

    /**
     * @Route("/help/asked/items/", name="helps_asked_vue")
     * @param FormatService $formatService
     * @return JsonResponse
     */
    public function getHelpAskedItems(FormatService $formatService): JsonResponse
    {
        $em = $this->getDoctrine()->getManager();
        $helpsNotAccepted = $em->getRepository(HelpAsked::class)->findHelpsByStatusAndUser(false, $this->getUser());
        $arrayHelps = $formatService->renderArrayHelps($helpsNotAccepted);

        return new JsonResponse(['helps' => $arrayHelps]);
    }
    /**
     * @Route("/help/accepted/detail/{id}", name="help_accepted_show")
     * @param $id
     * @return Response
     */
    public function show($id): Response
    {
        $helpAccepted = $this->getDoctrine()->getManager()->getRepository(HelpAccepted::class)->find($id);
        return $this->render('help_accepted/show.html.twig', [
            'help' => $helpAccepted
        ]);
    }

    /**
     * @Route("/help/accepted", name="help_accepted")
     */
    public function indexHelpsAccepted()
    {
        return $this->render('help_accepted/index.html.twig');
    }

    /**
     * @Route("/helped/accepted/add/{helpId}", name="help_accepted_add")
     *
     * @param Request $request
     * @param $helpId
     * @return JsonResponse
     */
    public function acceptHelp(Request $request, $helpId)
    {
        $em =$this->getDoctrine()->getManager();
        $help = $em->getRepository(HelpAsked::class)->find($helpId);

        $helpAccepted = new HelpAccepted();
        $helpAccepted->setHelp($help);
        $helpAccepted->setAcceptedBy($this->getUser());
        $em->persist($helpAccepted);
        $em->flush();

        $helps = (!empty($this->getUser())) ?$this->getDoctrine()->getRepository(HelpAsked::class)->findNotMyUser($this->getUser()) : null;

        return new JsonResponse([
            'id' => '#help_'.$helpId,
            'compteur' => count($helps),
            'messageEmpty' => "Il n'y a pas de demandes d'aide." ]);
    }

    /**
     * @Route("/helped/asked/add", name="helped_asked_add")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function add(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $form = $this->createForm(HelpAskedFormType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $datas = $form->getData();
            $addressComplete = $datas['address'];
            $addressArray = explode(',', $addressComplete);
            $number = substr(trim($addressArray['0']),0,strpos(trim($addressArray[0]),' '));
            $street = substr(trim($addressArray[0]), strpos(trim($addressArray[0]),' ')+1);
            $cp = substr(trim($addressArray[1]),0,strpos(trim($addressArray[1]),' '));
            $city = substr(trim($addressArray[1]), strpos(trim($addressArray[1]),' ')+1);

            $address = new Address();
            $address->setNumber($number);
            $address->setStreet($street);
            $address->setZipCode($cp);
            $address->setCity($city);
            $address->setCountry(trim($addressArray['2']));
            $address->setLat(floatval($datas['lat']));
            $address->setLng(floatval($datas['lng']));
            $em->persist($address);

            $helpAsked = new HelpAsked();
            $helpAsked->setTitle($datas['title']);
            $helpAsked->setContent($datas['content']);
            $helpAsked->setAddress($address);
            $helpAsked->setUser($this->getUser());
            $helpAsked->setCreated(new \DateTime());
            $em->persist($helpAsked);
            $em->flush();

            return $this->redirectToRoute('helped_asked');


        }
        return $this->render('helped_asked/add.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/help/asked/store", name="store_help_asked")
     */
    public function storeHelpAsked(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $date = new \DateTime($request->request->get('dateHelp'), new \DateTimeZone('UTC'));
        $date->setTimezone(new \DateTimeZone('Europe/Paris'));

        $addressComplete = $request->request->get('address');
        $addressArray = explode(',', $addressComplete);
        $number = substr(trim($addressArray['0']),0,strpos(trim($addressArray[0]),' '));
        $street = substr(trim($addressArray[0]), strpos(trim($addressArray[0]),' ')+1);
        $cp = substr(trim($addressArray[1]),0,strpos(trim($addressArray[1]),' '));
        $city = substr(trim($addressArray[1]), strpos(trim($addressArray[1]),' ')+1);

        $address = new Address();
        $address->setNumber($number);
        $address->setStreet($street);
        $address->setZipCode($cp);
        $address->setCity($city);
        $address->setCountry(trim($addressArray['2']));
        $address->setLat(floatval($request->request->get('lat')));
        $address->setLng(floatval($request->request->get('lng')));
        $em->persist($address);

        $helpAsked = new HelpAsked();
        $helpAsked->setTitle($request->request->get('title'));
        $helpAsked->setContent($request->request->get('content'));
        $helpAsked->setAddress($address);
        $helpAsked->setUser($this->getUser());
        $helpAsked->setCreated(new \DateTime());
        $helpAsked->setDateHelp($date);
        $em->persist($helpAsked);
        $em->flush();

        return new JsonResponse(['message' => 'Demande d\'aide bien enregistrée']);

    }
    /**
     * @Route("/help/accepted/delete/modal/{id}/{action}", name="admin_help_accepted_modal")
     * @param $id
     * @param $action
     * @return Response
     */
    public function generateModal($id, $action)
    {
        $em = $this->getDoctrine()->getManager();

        $params = [];
        $helpAccepted = $em->getRepository(HelpAccepted::class)->find($id);

        $params = [
            'helpAccepted' => $helpAccepted,
            'modalId' => 'lic_help_accepted_'.$action.'_modal'
        ];

        $html = $this->renderView('help_accepted/Modal/'.$action.'.html.twig', $params);

        return new Response($html);
    }

    /**
     * @Route("/help/asked/delete/{id}", name="delete_help_asked")
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function deleteHelpAccepted($id)
    {
        $em = $this->getDoctrine()->getManager();
        $helpAccepted = $em->getRepository(HelpAccepted::class)->find($id);
        $title= $helpAccepted->getHelp()->getTitle();

        if (!empty($helpAccepted)) {
            $em->remove($helpAccepted);
            $em->flush();
        }
        $helpsAccepted = $em->getRepository(HelpAccepted::class)->findBy(['acceptedBy' => $this->getUser()]);

        return new JsonResponse([
            'success' => true,
            'id' => '#help_'.$helpAccepted->getHelp()->getId(),
            'message' => 'La proposition: '.$title.' a bien été supprimée !',
            'compteur' => count($helpsAccepted),
            'messageEmpty' => "Vous n'avez pas encore accepté de demandes d'aide."
        ]);

    }
}
