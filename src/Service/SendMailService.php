<?php

namespace App\Service;

use Exception;
use Swift_Mailer;

class SendMailService
{
    /**
     * @var \Swift_Mailer
     */
    private $swiftMailer;

    /**
     * SendMailService constructor.
     * @param \Swift_Mailer $swiftMailer
     */
    public function __construct(Swift_Mailer $swiftMailer)
    {
        $this->swiftMailer = $swiftMailer;
    }

    public function sendEmail($to, $message, $subject)
    {
        try {
            $message = (new \Swift_Message($subject))
                ->setContentType('text/html')
                ->setFrom('n.bermond@orange.fr')
                ->setTo(trim($to))
                ->setBody($message);

            $this->swiftMailer->send($message);
        }
        catch (Exception $e) {
            return $e->getMessage();
        }

        return 'Message bien envoyé';
    }
}