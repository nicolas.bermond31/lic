<?php


namespace App\Service;


use App\Entity\HelpAsked;
use App\Entity\Remark;
use Symfony\Component\Routing\Router;

class FormatService
{

    /**
     * @var Router
     */
    private $router;

    /**
     * FormatService constructor.
     * @param Router $router
     */
    public function __construct(Router $router)
    {
        $this->router = $router;
    }

    public function renderArrayRemarks($resultsRemarks, $user)
    {
        $arrayRemarks = [];
        foreach ($resultsRemarks as $resultRemark) {
            /**
             * @var Remark $resultRemark
             */
            $arrayRemarks [] = [
                'idHelpAccepted' => $resultRemark->getHelpAccepted()->getId(),
                'user' => $resultRemark->getUser()->fullName(),
                'userId'=> $resultRemark->getUser()->getId(),
                'created' => $resultRemark->getCreated()->format('d/m/Y'),
                'content' => $resultRemark->getContent(),
                'userLogged' => $user->getId()
            ];
        }

        return $arrayRemarks;
    }
    public function renderArrayHelps($resultHelps, $parent = 'index')
    {
        $arrayHelps= [];
        foreach ($resultHelps as $resultHelp){
            /**
             * @var HelpAsked $resultHelp
             */
            if ($parent === 'index') {
                $arrayHelps[]=[
                    'id' => $resultHelp->getId(),
                    'title' => $resultHelp->getTitle(),
                    'content' => $resultHelp->getContent(),
                    'dateHelp'=> !empty($resultHelp->getDateHelp()) ? $resultHelp->getDateHelp()->format(\DateTime::ATOM) : '',
                    'address' => [
                        'number' =>$resultHelp->getAddress()->getNumber(),
                        'street' =>$resultHelp->getAddress()->getStreet(),
                        'street2' =>$resultHelp->getAddress()->getStreet2(),
                        'zipCode' =>$resultHelp->getAddress()->getzipCode(),
                        'city' =>$resultHelp->getAddress()->getCity(),
                        'lat'=>$resultHelp->getAddress()->getLat(),
                        'lng' => $resultHelp->getAddress()->getLng()
                    ],
                    'addLink' => $this->router->generate('help_accepted_add', ['helpId'=> $resultHelp->getId()]),
                    'btnIcon' => 'plus',
                    'btnClasses' => 'btn btn-success btn-lg white-link',
                    'btnText' => 'Accepter la demande',
                    'btnMessage' => 'Demande bien acceptée !'
                ];
            }

            if ($parent === 'accepted') {
                $arrayHelps[]=[
                    'id' => $resultHelp->getHelp()->getId(),
                    'title' => $resultHelp->getHelp()->getTitle(),
                    'content' => $resultHelp->getHelp()->getContent(),
                    'dateHelp'=> !empty($resultHelp->getHelp()->getDateHelp()) ? $resultHelp->getHelp()->getDateHelp()->format(\DateTime::ATOM) : '',
                    'address' => [
                        'number' =>$resultHelp->getHelp()->getAddress()->getNumber(),
                        'street' =>$resultHelp->getHelp()->getAddress()->getStreet(),
                        'street2' =>$resultHelp->getHelp()->getAddress()->getStreet2(),
                        'zipCode' =>$resultHelp->getHelp()->getAddress()->getzipCode(),
                        'city' =>$resultHelp->getHelp()->getAddress()->getCity(),
                        'lat'=>$resultHelp->getHelp()->getAddress()->getLat(),
                        'lng' => $resultHelp->getHelp()->getAddress()->getLng()
                    ],
                    'addLink' => $this->router->generate('delete_help_asked', ['id'=> $resultHelp->getId()]),
                    'showLink' => $this->router->generate('help_accepted_show', ['id' => $resultHelp->getId()]),
                    'btnIcon' => 'trash',
                    'btnClasses' => 'delete btn btn-danger btn-lg',
                    'btnText' => 'Supprimer',
                    'btnMessage' => 'Proposition d\'aide bien supprimée '
                ];
            }

        }

        return $arrayHelps;
    }
}