<?php


namespace App\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class HelpAskedFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('title', TextType::class, [
            'attr' => [
                'class' => 'form-control',
                'id' => 'title-input'
            ],
            'label' => 'Titre de la demande',
            'required' => true
        ])
            ->add('content', TextareaType::class,[
                'attr' => [
                    'class' => 'form-control',
                    'id' => 'content-input'
                ],
                'label' => 'Texte de l\'annonce'
            ])
            ->add('address', TextType::class, [
                'attr' => [
                    'class' => 'form-control',
                    'id' => 'address-input',
                    'onkeyup'=>'autosuggest(this)'
                ],
                'label' => 'Lieu où aider',
                'required' => true
            ])
            ->add('lat', HiddenType::class, [
                'attr' => [
                    'id' => 'lat-input'
                ]
            ])
            ->add('lng', HiddenType::class, [
                'attr' => [
                    'id' => 'lng-input'
                ]
            ])
        ;

    }
}