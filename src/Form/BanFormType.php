<?php


namespace App\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class BanFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('ban', ChoiceType::class, [
            'attr' => [
                'class' => 'form-control',
                'id' => 'ban'
            ],
            'label' => 'Type de bannissement',
            'choices' => [
                'veuillez choisir un type de bannisssement' => '',
                'Temporaire' => 'temp',
                'Définitif' => 'forever'
            ],
            'multiple' => false,
            'expanded' => false
        ])
        ;

    }
}