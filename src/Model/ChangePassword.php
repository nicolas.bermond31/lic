<?php

namespace App\Model;

use Symfony\Component\Security\Core\Validator\Constraints as SecurityAssert;
class ChangePassword
{
    /**
     * @SecurityAssert\UserPassword(
     *     message = "Mauvaise valeur pour votre mot de passe actuel"
     * )
     */
    protected $oldPassword;

    protected $password;

    /**
     * @return mixed
     */
    function getOldPassword() {
        return $this->oldPassword;
    }

    /**
     * @return mixed
     */
    function getPassword() {
        return $this->password;
    }

    /**
     * @param $oldPassword
     * @return $this
     */
    function setOldPassword($oldPassword) {
        $this->oldPassword = $oldPassword;
        return $this;
    }

    /**
     * @param $password
     * @return $this
     */
    function setPassword($password) {
        $this->password = $password;
        return $this;
    }
}